from discord.ext import commands
from loguru import logger


def _is_admin(self, ctx: commands.Context):
    """
    Get the channel for a channel_name.
    paramters:
        channel_name:        The channel where to fetch channel for.
    """
    admin_roles = ["Admin", "Moderator", "Bot Bouwers"]
    for role in admin_roles:
        if self._in_role(ctx, role):
            logger.info(role)
            return True
    return False

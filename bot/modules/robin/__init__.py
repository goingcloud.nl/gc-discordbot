"""
This file contains the main functions and classes for Robin.
"""
from discord.ext import commands


class Robin(commands.Cog):
    """
    The master class for Robin.
    """

    from ._feedback import _feedback
    from ._get_channel_by_name import _get_channel_by_name
    from ._get_channel_id_by_name import _get_channel_id_by_name
    from ._get_role_by_name import _get_role_by_name
    from ._get_role_id_by_name import _get_role_id_by_name
    from ._get_rolemembers import _get_rolemembers
    from ._get_userid import _get_userid
    from ._get_username_by_id import _get_username_by_id
    from ._getusermap import _getusermap
    from ._has_gsalias import _has_gsalias
    from ._in_role import _in_role
    from ._is_admin import _is_admin
    from ._sanitize import _sanitize

    def __init__(self, bot=None, db=None):
        self.bot = bot
        self.db = db
        # logger.info(f"Class {type(self).__name__} initialized from Robin class")

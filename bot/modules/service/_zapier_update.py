"""
The contents of this file is to reflect happyness of robin.
"""

from loguru import logger


########################################################################################
#  zapier update
########################################################################################
async def _zapier_update(self, message):
    lijst = message.content.split()
    if lijst[0] == ",inputmodules":
        if len(lijst) == 73:
            lijst.pop(7)
            lijst.pop(6)
            lijst.pop(1)
            lijst.pop(0)

            if self._update_gsheet_entry(lijst):
                await message.delete()
                logger.info(f"deleted message {message.content}")
            else:
                await message.channel.send(
                    f"Ik kan de modules:\n `{message.content}`\n "
                    "niet updaten in de database, misschien slim om even na te kijken "
                    "of de velden allemaal wel correct zijn.."
                )
        else:
            await message.channel.send(
                f"Niet genoeg velden in:\n `{message.content}`\n,"
                " heb je handmatig zitten prutsen?"
            )

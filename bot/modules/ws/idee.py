"""
This file is for the idea related functions
"""

from discord.ext import commands
from loguru import logger

from ..utils import feedback, get_channel_by_name, get_role_id, in_role, sanitize

########################################################################################
#  command idee
########################################################################################


def _get_ws(ctx: commands.Context, settings):
    """
    Get the ws context

    Args:
    ----
        ctx (commands.Context): [command context]

    """
    logger.info(f"settings: {settings}")
    # logger.info(f"testje get {in_role()}")
    if in_role(ctx, "WS1"):
        logger.info("ws1 reached")
        ws = settings.get("WS1")
    elif in_role(ctx, "WS2"):
        logger.info("ws2 reached")
        ws = settings.get("WS2")
    else:
        logger.info("None reached")
        logger.info("None")
        ws = None
    logger.info(f"ws: {ws}")
    return ws


@commands.command(
    name="idee",
    help=(
        "Als je een idee hebt kun je dit hiermee doorgeven,"
        " de planners krijgen dit in het planning kanaal te zien."
    ),
    brief="Geef een idee door aan de planners.",
)
async def idee(self, ctx, *args):
    """
    Posting an idea in the plannnig channel
    """
    if ws := _get_ws(ctx=ctx, settings=self.bot.settings):
        usermap = self._getusermap(int(ctx.author.id))
        idea = sanitize(" ".join(args), 100)
        officers_channel = get_channel_by_name(ctx, ws.get("OFFICERS_CHANNEL_NAME"))
        planner_role_id = get_role_id(ctx, ws.get("PLANNER_ROLE_NAME"))

        if ctx.channel != get_channel_by_name(ctx, ws.get("CHANNEL_NAME")):
            message = f"{usermap['DiscordAlias']} post je idee in het ws-kanaal"
        else:
            logger.info(f"officers_channel: {officers_channel}")
            message = (
                f"<@&{planner_role_id}> idee van {usermap['DiscordAlias']}: {idea}"
            )
            await officers_channel.send(message)

            message = (
                f"Dank, {usermap['DiscordAlias']} je is doorgestuurd aan de officers"
            )

    await feedback(ctx=ctx, msg=message, delete_after=3)

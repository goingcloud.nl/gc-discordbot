#!/usr/bin/env python


from datetime import datetime

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, Table

from . import Base


class WSTemp(Base):
    __tablename__ = "wstemp"
    UserId = Column(Integer, ForeignKey("user.UserId"), primary_key=True)

    def __repr__(self):
        return f"<WSTemp(UserId={self.UserId}>"

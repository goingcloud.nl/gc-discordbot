#!/usr/bin/env python


from datetime import datetime

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, Table

from . import Base


class User(Base):
    __tablename__ = "user"
    UserId = Column(Integer, primary_key=True)
    DiscordId = Column(String)
    DiscordAlias = Column(String)
    GsheetAlias = Column(String)
    LastActive = Column(DateTime, default=datetime.now, onupdate=datetime.now)
    LastChannel = Column(String)

    def __repr__(self):
        return (
            f"<User(UserId={self.UserId},"
            f"DiscordId={self.DiscordId},"
            f"DiscordAlias={self.DiscordAlias},"
            f"GsheetAlias={self.GsheetAlias},"
            f"LastActive={self.LastActive},"
            f"LastChannel={self.LastChannel})>"
        )

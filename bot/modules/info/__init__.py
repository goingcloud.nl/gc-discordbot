"""
This file contains the main functions and classes for Robin.
"""
from loguru import logger

from ..robin import Robin


class Info(Robin):
    """
    The master class for Robin.
    """

    from ._generate_module_image import _generate_module_image, _parse_module_image
    from ._generate_wsmodule_image import (
        _generate_wsmodule_image,
        _parse_wsmodule_image,
    )
    from ._module_data import (
        MINING_IMAGES,
        MODULE_IMAGES,
        SHIELD_IMAGES,
        SUPPORT_IMAGES,
        TRADE_IMAGES,
        WEAPON_IMAGES,
    )
    from .info import info, prefix_info, slash_info
    from .mining import mining
    from .shield import shield
    from .support import support
    from .trade import trade
    from .weapon import weapon

    def __init__(self, bot=None, db=None):
        super().__init__()
        self.bot = bot
        self.db = db
        logger.info(f"Class {type(self).__name__} initialized from Robin class")

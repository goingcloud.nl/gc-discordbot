import bot.modules.db as db


def temp_db_decorator(func):
    def inner(*args, **kwargs):
        db.session = db.init("sqlite:///:memory:")
        # print("I can decorate any function")
        func(*args, **kwargs)
        db.session.close()

    return inner


async def async_temp_db_decorator(func):
    async def inner(*args, **kwargs):
        db.session = db.init("sqlite:///:memory:")
        # print("I can decorate any function")
        await func(*args, **kwargs)
        db.session.close()

    return inner
